html:
	./generator.py > index.html
upload:
	rsync -av --delete\
		index.html style.css abstracts handouts images presentations supplementary notes assignments posters tmp digital personal media spiels cv.pdf\
		acdigitalwordsnet@digitalwords.net:ac.digitalwords.net --cvs-exclude
