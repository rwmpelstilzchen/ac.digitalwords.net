#!/usr/bin/env python

import datetime
import dominate
from dominate import tags
from dominate.util import raw
import markdown
import os
import yaml

mat = {
        'presentation': {
            'icon': 'document-landscape.svg',
            'text': 'presentation',
            'path': 'presentations'
            },
        'handout': {
            'icon': 'open-book.rotated.svg',
            'text': 'handout',
            'path': 'handouts'
            },
        'notes': {
            'icon': 'text-document.svg',
            'text': 'notes',
            'path': 'notes'
            },
        'abstract': {
            'icon': 'text.svg',
            'text': 'abstract',
            'path': 'abstracts'
            },
        'source': {
            'icon': 'code.svg',
            'text': 'source',
            },
        'copy': {
            'icon': 'arrow-with-circle-down.svg',
            'text': 'digital copy',
            'path': 'digital'
            },
        'assignment': {
            'icon': 'check.svg',
            'text': 'assignment',
            'path': 'assignments'
            },
        'spiel': {
            'icon': 'message.svg',
            'text': 'spiel',
            'path': 'spiels'
            },
        'poster': {
            'icon': 'document.svg',
            'text': 'poster',
            'path': 'posters'
            },
        'media': {
            'icon': 'arrow-with-circle-down.svg',
            'text': 'media',
            'path': 'media'
            },
        'webpage': {
            'icon': 'globe.svg',
            'text': 'webpage',
            'path': ''  # remove?
            },
        }

rowcounter = 0

def thesis_func(row):
    if 'degree' in row:
        tags.td(raw(row['degree']))
    if 'advisors' in row:
        tags.td(raw(row['advisors']))
    if 'institution' in row:
        tags.td(raw(row['institution']))

def article_func(row):
    with tags.td():
        if 'journalurl' in row:
            tags.a(row['journal'], href=row['journalurl'])
        else:
            row['journal']
        raw(" ")
        if 'volumeurl' in row:
            tags.a(str(row['volume']), href=row['volumeurl'])
        else:
            raw(str(row['volume']))
        if 'issue' in row:
            raw("/")
            if 'issueurl' in row:
                tags.a(str(row['issue']), href=row['issueurl'], __inline=True)
            else:
                raw(row['issue'])
        if 'pages' in row:
            raw(", pp.&nbsp;" + row['pages'])
    if 'doi' in row:
        tags.td(tags.a(str(row['doi']), href="https://doi.org/" + str(row['doi'])))
    with tags.td():
        with tags.div(style="white-space: nowrap;"):
            if 'open-access' in row['licence']:
                with tags.a(href="https://en.wikipedia.org/wiki/Open_access"):
                    tags.img(src='images/icons/open-access.svg', cls='icon')
            if 'cc-by-nc-nd' in row['licence']:
                with tags.a(href="https://creativecommons.org/licenses/by-nc-nd/4.0/"):
                    tags.img(src='images/icons/cc.svg', cls='icon')
                    tags.img(src='images/icons/cc-by.svg', cls='icon')
                    tags.img(src='images/icons/cc-nc.svg', cls='icon')
                    tags.img(src='images/icons/cc-nd.svg', cls='icon')
        if 'degruyter-open' in row['licence']:
            tags.small(tags.a("De Gruyer Open", href="http://www.degruyteropen.com"))


def conf_func(row):
    if 'eventurl' in row:
        tags.td(tags.a(raw(row['event']), href=row['eventurl']))
    else:
        tags.td(raw(row['event']))
    if 'venue' in row:
        venuetext = row['venue']['name']
        if 'location' in row:
            venuetext += ", " + row['location']
        with tags.td():
            if 'url' in row['venue']:
                tags.a(venuetext, href=row['venue']['url'])
            else:
                tags.span(venuetext)
    else:
        if 'location' in row:
            tags.td(row['location'])
        else:
            tags.td()

def course_func(row):
    with tags.td():
        if 'courseurl' in row:
            tags.a(row['course'], href=row['courseurl'])
        else:
            tags.span(row['course'])
    with tags.td():
        tags.span(row['institution'])

def seminar_func(row):
    with tags.td():
        if 'seminarurl' in row:
            tags.a(row['seminar'], href=row['seminarurl'])
        else:
            tags.span(row['seminar'])
        if 'course' in row:
            tags.span(f" ({row['course']})")

def seminarpaper_func(row):
    if 'degree' in row:
        tags.td(raw(row['degree']))
    if 'advisor' in row:
        tags.td(raw(row['advisor']))


def make_table(filename, special_function, tableid):
    global rowcounter
    data = list(yaml.load_all(open('yaml/' + filename + '.yaml'), Loader=yaml.SafeLoader))
    if 'title' in data[0]:
        print(tags.h3(raw(data[0]['title']), id=data[0]['slug']))
    with tags.table(id=tableid+'-table') as t:
        with tags.tr():
            for th in data[0]['header']:
                tags.th(th['th'])
        for row in data[1]:
            anchor = data[0]['slug'] + "-" + str(row['date'])
            with tags.tr(id=anchor):
                if type(row['date']) == datetime.date:
                    with tags.td(title=f"{row['date'].year}/{row['date'].month}/{row['date'].day}"):
                        tags.a(row['date'].year, href="#" + anchor, cls="bodylink")
                else:
                    with tags.td():
                        tags.a(row['date'], href="#" + anchor, cls="bodylink")
                with tags.td(onclick=f"$('#{rowcounter}-abstract').slideToggle();"):
                    raw(row['title'])
                    if 'language' in row:
                        tags.span(f"[{row['language']}]", cls='lang')
                    if 'abstract' in row:
                        with tags.div(id=f"{rowcounter}-abstract", hidden=True):
                            tags.h4("Abstract")
                            raw(markdown.markdown(row['abstract'], extensions=['extra']))
                special_function(row)
                with tags.td():
                    if 'materials' in row:
                        for material in row['materials']:
                            m = mat[material['type']]
                            if 'url' in material:
                                url = material['url']
                            else:
                                if material['type'] == 'media':
                                    url = f"{m['path']}/{row['date'].year:02d}-{row['date'].month:02d}-{row['date'].day:02d}/"
                                else:
                                    url = f"{m['path']}/{row['date'].year:02d}-{row['date'].month:02d}-{row['date'].day:02d}.pdf"
                            with tags.a(href=url, style='white-space: nowrap;'):
                                tags.img(src=f'images/icons/{m["icon"]}', cls='icon')
                                tags.span(m['text'], style='font-variant: small-caps')
                            tags.br()
                    ext_abstract = None
                    if 'materials' in row:
                        ext_abstract = next((l for l in row['materials'] if l['type'] == 'abstract'), None)
                    if 'abstract' in row and not ext_abstract:
                        with tags.a(onclick=f"$('#{rowcounter}-abstract').slideToggle();", style='white-space: nowrap;'):
                            tags.img(src=f'images/icons/text.svg', cls='icon')
                            tags.span("abstract", style='font-variant: small-caps')
            rowcounter += 1
    print(t)


print(open('header.html').read())

print(tags.h2("Theses", id="theses"))
make_table('theses', thesis_func, 'theses')

#print(tags.h2("Publications", id="publications"))
#make_table('articles', article_func, 'articles')

print(tags.h2("Teaching", id="teaching"))
make_table('courses', course_func, 'courses')


print(tags.h2("Presentations", id="presentations"))
make_table('talks', conf_func, 'talks')
make_table('posters', conf_func, 'posters')
make_table('seminars', seminar_func, 'seminars')
make_table('nonacademic', conf_func, 'nonacademic')

print(tags.h2("Seminar papers", id="seminarpapers"))
make_table('seminarpapers', seminarpaper_func, 'seminarpapers')


print(open('colophon.html').read())
print(tags.span("Last update: " + datetime.datetime.today().strftime('%Y-%m-%d')))
print(open('footer.html').read())
